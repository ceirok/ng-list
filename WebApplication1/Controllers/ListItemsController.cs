﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Data;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class ListItemsController : Controller
    {
        private readonly TestContext _context;

        public ListItemsController(TestContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var list = await _context.ListItems.ToArrayAsync();
            return Ok(list);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var item = await _context.ListItems.FindAsync(id);
            _context.Remove(item);

            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}
