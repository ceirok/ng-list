import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './listItems.component.html'
})
export class ListItemsComponent {
  public items: IListItem[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<IListItem[]>(`${baseUrl}api/ListItems`)
      .subscribe(result => {
        this.items = result;
      }, error => console.error(error));
  }
}

interface IListItem {
  id: string;
  name: number;
}
