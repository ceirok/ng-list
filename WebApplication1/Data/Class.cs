﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Data
{
    public class TestContext : DbContext
    {
        public DbSet<ListItem> ListItems { get; set; }

        public TestContext(DbContextOptions<TestContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ListItem>()
                .Property(x => x.Name)
                .HasMaxLength(500);
        }
    }

    public class ListItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
